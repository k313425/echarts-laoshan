# 0828_laoshan

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### 效果图
![新窗口打开](https://gitee.com/k313425/echarts-laoshan/raw/master/public/img.jpg)