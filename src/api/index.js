import request from '@/utils/request'

//首页图表数据
export function listCarType(data) {
  return request({
    url: '/api/',
    method: 'post',
    data: data
  })
}

//首页天气数据
export function weather(data) {
  return request({
    url: '/api/',
    method: 'post',
    data: data
  })
}
