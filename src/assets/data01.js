let dataCandle = {
    grid: {
        top: 40,
        bottom: 30,
    },
    xAxis: {
        type: 'category',
        axisLabel: {
            interval: 0,
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        data: ['沙子口街道', '北宅街道', '王哥庄街道', '金家岭街道', '中韩街道']
    },
    yAxis: {
        type: 'value',
        name: '单位：人数',
        nameTextStyle: {
            color: '#fff'
        },
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            },
        }
    },
    series: {
        data: [50, 70, 86, 80, 52, 51],
        type: 'bar',
        itemStyle: {
            color: '#34abff'
        },
        barWidth: 25,
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#eb46a1'
            },
            itemStyle: {
                color: '#eb46a1'
            },
        }
    }
}
export default dataCandle