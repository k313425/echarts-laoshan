let dataCandle = {
    radar: {
        indicator: [
            {text: '0-10岁', max: 100},
            {text: '11-20岁', max: 100},
            {text: '21-30岁', max: 100},
            {text: '31-40岁', max: 100},
            {text: '40岁以上', max: 100}
        ],
        radius: 70,
        splitArea: {
            show: false
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            }
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            }
        },
        name: {
            textStyle: {
                color: '#fff'
            }
        },
    },
    series: {
        type: 'radar',
        // symbol: 'none', //拐点
        tooltip: {
            trigger: 'item'
        },
        label: true,
        lineStyle: {
            color: '#b36d1f',
        },
        areaStyle: {
            opacity: 1,
            color: '#b36d1f'
        },
        data: [
            {
                value: [60, 82, 62, 40, 42],
                name: '党龄',
                label: {
                    show: true,
                    color: '#fff',
                    formatter: '{c}' + '人',
                }
            }
        ]
    }
}
export default dataCandle