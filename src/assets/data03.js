let dataCandle = {
    grid: {
        left: 80,
        top: 40,
        right: 50,
        bottom: 30,
    },
    xAxis: {
        type: 'category',
        axisLabel: {
            interval: 0,
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        data: ['困难党员', '年老体弱党员', '流动党员', '失联党员', '出国党员', '受表彰党员']
    },
    yAxis: {
        type: 'value',
        name: '单位：人数',
        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
            color: '#fff'
        },
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            },
        }
    },
    series: {
        data: [50, 70, 86, 80, 52, 51, 62],
        type: 'bar',
        itemStyle: {
            color: '#34abff'
        },
        barWidth: 25,
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#ff9745'
            },
            itemStyle: {
                color: '#ff9745'
            },
        }
    }
}
export default dataCandle