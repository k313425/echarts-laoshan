let dataCandle = {
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b}: {c} ({d}%)'
    },
    color:['#4b3fe2', '#f7807c'],
    legend: {
        bottom: 5,
        right: 10,
        data: ['男', '女'],
        textStyle: {
            color: '#fff'
        }
    },
    series: {
        name: '男女比例',
        type: 'pie',
        radius: ['30%', '50%'],
        avoidLabelOverlap: false,
        label: {
            formatter: '{d}%',
            color: '#fff'
        },
        labelLine: {
            length: 0,
            length2: 0
        },
        data: [
            {value: 143, name: '男'},
            {value: 117, name: '女'},
        ]
    }
}
export default dataCandle