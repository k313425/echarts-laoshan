let dataCandle = {
    grid: {
        left: 90,
        top: 40,
        right: 30,
        bottom: 30,
    },
    xAxis: {
        type: 'value',
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
    },
    yAxis: {
        type: 'category',
        data: ['入党积极分子', '预备党员', '正常党员'],
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
    },
    series: {
        data: [120, 80, 120],
        type: 'bar',
        itemStyle: {
            color: function(params) {
                let colorList = [
                    '#12ffbb','#ff8a11','#1296ff'
                ];
                return colorList[params.dataIndex]
            }
        },
        barWidth: 25,
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#a6cdff'
            },
            itemStyle: {
                color: '#a6cdff'
            },
        }
    }
}
export default dataCandle