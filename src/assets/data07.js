let dataCandle = {
    grid: {
        left: 70,
        top: 40,
        right: 50,
        bottom: 30,
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        axisLabel: {
            interval: 0,
            fontSize: 11,
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#ff9745'
            },
        },
        data: ['批评教育', '限期改正', '劝退', '除名', '党内警告', '严重警告', '撤销党内职务', '留党察看', '开除党籍']
    },
    yAxis: {
        type: 'value',
        name: '单位：人数',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
            color: '#fff'
        },
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#ff9745'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#ff9745'
            },
        }
    },
    series: {
        data: [45, 28, 15, 28, 70, 28, 44, 35, 11],
        type: 'line',
        itemStyle: {
            color: '#ff9745'
        },
        areaStyle: {
            color: '#ff9745'
        },
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#34abff'
            },
            itemStyle: {
                color: '#34abff'
            },
        }
    }
}
export default dataCandle