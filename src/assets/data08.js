let dataCandle = {
    grid: {
        left: 80,
        top: 40,
        right: 50,
        bottom: 30,
    },
    xAxis: {
        type: 'category',
        axisLabel: {
            interval: 0,
            fontSize: 11,
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        data: ['全国党代表', '省党代表', '市党代表', '区党代表', '全国人大代表', '省人大代表', '市人大代表', '区人大代表', '全国政协委员', '省政协委员', '市政协委员', '区政协委员']
    },
    yAxis: {
        type: 'value',
        name: '单位：人数',
        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
            color: '#fff'
        },
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            },
        }
    },
    series: {
        data: [50, 70, 86, 80, 52, 51, 62, 86, 80, 52, 51, 62],
        type: 'bar',
        itemStyle: {
            color: '#34abff'
        },
        barWidth: 25,
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#ff9745'
            },
            itemStyle: {
                color: '#ff9745'
            },
        }
    }
}
export default dataCandle