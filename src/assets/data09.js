let dataCandle = {
    grid: {
        left: 80,
        top: 40,
        right: 50,
        bottom: 30,
    },
    xAxis: {
        type: 'category',
        axisLabel: {
            interval: 0,
            fontSize: 11,
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        data: ['博士研究生', '硕士研究生', '大学本科', '大专', '高中', '中专、技校', '初中', '小学及以下']
    },
    yAxis: {
        type: 'value',
        name: '单位：人数',
        nameLocation: 'center',
        nameGap: 40,
        nameTextStyle: {
            color: '#fff'
        },
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#34abff'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#34abff'
            },
        }
    },
    series: {
        data: [50, 70, 86, 80, 52, 51, 62, 86],
        type: 'bar',
        itemStyle: {
            color: '#34abff'
        },
        barWidth: 25,
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#00ffb4'
            },
            itemStyle: {
                color: '#00ffb4'
            },
        }
    }
}
export default dataCandle