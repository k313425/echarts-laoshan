let dataCandle = {
    grid: {
        left: 70,
        top: 40,
        right: 50,
        bottom: 30,
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            label: {
                backgroundColor: '#6a7985'
            }
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        axisLabel: {
            interval: 0,
            fontSize: 11,
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#00ffb4'
            },
        },
        data: ['机关事业单位', '社会组织职工', '国有企业职工', '非公有企业职工', '城市社区居民', '农村社区居民', '其它']
    },
    yAxis: {
        type: 'value',
        name: '单位：人数',
        nameLocation: 'center',
        nameGap: 30,
        nameTextStyle: {
            color: '#fff'
        },
        axisLabel: {
            color: '#fff'
        },
        axisLine: {
            lineStyle: {
                color: '#00ffb4'
            },
        },
        splitLine: {
            lineStyle: {
                color: '#00ffb4'
            },
        }
    },
    series: {
        data: [45, 28, 25, 28, 70, 28, 44],
        type: 'line',
        itemStyle: {
            color: '#00ffb4'
        },
        areaStyle: {
            color: '#00ffb4'
        },
        emphasis: {
            label: {
                show: true,
                position: 'top',
                formatter: '{c}' + '人',
                color: '#34abff'
            },
            itemStyle: {
                color: '#34abff'
            },
        }
    }
}
export default dataCandle